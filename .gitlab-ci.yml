---
stages:
  - pre-commit
  - test-suite
  - test-real-k8s
  - pkg
  - pop-release

include:
  - project: saltstack/pop/cicd/ci-templates
    file: /lint/pre-commit-run-all.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /release/pop_release.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/url-tester-brok.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/build-docs-html-nox.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/publish-docs-gitlab.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/publish-docs-release.yml

variables:
  CICD_UPSTREAM_PATH: "vmware/idem/idem-k8s"

pre-commit-run-all:
  image: registry.gitlab.com/saltstack/pop/cicd/containers/alpinelint:py39

.test-suite:
  stage: test-suite
  needs:
    - pre-commit-run-all
  script:
    - python3 -m pip install nox
    - nox -e tests-3 -- -vv tests/unit
  artifacts:
    when: always
    paths:
      - artifacts
    reports:
      junit: artifacts/junit-report.xml
    expire_in: 30 days

tests-3.7:
  extends: .test-suite
  image: python:3.7

tests-3.8:
  extends: .test-suite
  image: python:3.8

tests-3.9:
  extends: .test-suite
  image: python:3.9

tests-3.10:
  extends: .test-suite
  image: python:3.10.0

# Run this on real k8s
# In gitlab go to your fork of idem-k8s -> Settings -> CI / CD -> Variables
# Create a File key called "CI_ACCT_FILE" that contains the contents of your encrypted fernet file
# Create a Variable key called "CI_ACCT_KEY" that contains the key for decrypting your acct file
# Create a Variable key called "CI_KUBE_CONFIG" that contains the contents of kubernetes config file
# TODO do some before and after things, like before the entire test starts list EVERYTHING
#     When the test is over list EVERYTHING again.
#     If there were any changes to real k8s after the tests (because tests didn't clean up properly or removed stuff)
#     then explode and throw a ton of descriptive errors
.test-suite-real:
  stage: test-real-k8s
  needs:
    - pre-commit-run-all
    - tests-3.7
  artifacts:
    when: always
    paths:
      - artifacts
    reports:
      junit: artifacts/junit-report.xml
    expire_in: 30 days
  image: python:3.7
  when: manual

tests-core:
  extends: .test-suite-real
  script:
    - export ACCT_KEY="$CI_ACCT_KEY"
    - export ACCT_FILE="$CI_ACCT_FILE"
    - python3 -m pip install nox
    - nox -e tests-3 -- -vv tests/integration/idem_k8s/states/k8s/core/v1/

tests-app:
  extends: .test-suite-real
  script:
    - export ACCT_KEY="$CI_ACCT_KEY"
    - export ACCT_FILE="$CI_ACCT_FILE"
    - python3 -m pip install nox
    - nox -e tests-3 -- -vv tests/integration/idem_k8s/states/k8s/apps/v1/

tests-rbac:
  extends: .test-suite-real
  script:
    - export ACCT_KEY="$CI_ACCT_KEY"
    - export ACCT_FILE="$CI_ACCT_FILE"
    - python3 -m pip install nox
    - nox -e tests-3 -- -vv tests/integration/idem_k8s/states/k8s/rbac/v1/

tests-storage:
  extends: .test-suite-real
  script:
    - export ACCT_KEY="$CI_ACCT_KEY"
    - export ACCT_FILE="$CI_ACCT_FILE"
    - python3 -m pip install nox
    - nox -e tests-3 -- -vv tests/integration/idem_k8s/states/k8s/storage_k8s_io/v1/

tests-api-registration:
  extends: .test-suite-real
  script:
    - export ACCT_KEY="$CI_ACCT_KEY"
    - export ACCT_FILE="$CI_ACCT_FILE"
    - python3 -m pip install nox
    - nox -e tests-3 -- -vv tests/integration/idem_k8s/states/k8s/apiregistration_k8s_io/v1/

build-docs-html:
  stage: test-suite

pages:
  variables:
    CICD_DOCS_VERSION_LATEST: latest
  stage: pkg

publish-docs:
  variables:
    CICD_S3_DEST_PATH: "docs.idemproject.io/idem-k8s/"
