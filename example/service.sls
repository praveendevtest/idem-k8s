test_service-nodeport:
  k8s.core.v1.service.present:
    - resource_id: test-service-nodeport
    - metadata:
        name: "test-service-nodeport"
        labels:
          name: test-service-nodeport
        namespace: default
    - spec:
        type: NodePort
        selector:
          app: nginx
        ports:
          - nodePort: 30163
            port: 8080
            targetPort: 80

test_service-lb:
  k8s.core.v1.service.present:
    - metadata:
        name: "test_service-lb"
        labels:
          name: test_service-lb
        namespace: default
    - spec:
        type: LoadBalancer
        selector:
          app: nginx
        ports:
          - protocol: TCP
            port: 80
            targetPort: 9376

cluster-ip-1:
  k8s.core.v1.service.present:
    - metadata:
        name: "cluster-ip"
        labels:
          name: cluster-ip
        namespace: default
    - spec:
        type: ClusterIP
        selector:
          app: nginx
        ports:
          - targetPort: 9376
            port: 80
