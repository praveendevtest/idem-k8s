import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_secret(hub, ctx):
    k8s_secret_temp_name = "idem-test-k8s-secret-" + str(uuid.uuid4())
    metadata_name = k8s_secret_temp_name
    metadata = {
        "name": metadata_name,
        "labels": {"name": "development-1"},
        "namespace": "default",
    }
    data = {"username": "YWRtaW4=", "password": "MWYyZDFlMmU2N2Rm"}

    # create k8s_secret with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.k8s.core.v1.secret.present(
        test_ctx, name=k8s_secret_temp_name, metadata=metadata, data=data, type="Opaque"
    )
    assert ret["result"], ret["comment"]
    assert f"Would create k8s.core.v1.secret '{k8s_secret_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert_secret(
        data,
        metadata,
        metadata_name,
        ret.get("new_state"),
        "development-1",
        "resource_id_known_after_present",
    )

    # create real k8s_secret
    ret = await hub.states.k8s.core.v1.secret.present(
        ctx, name=k8s_secret_temp_name, metadata=metadata, data=data, type="Opaque"
    )
    assert ret["result"], ret["comment"]
    assert f"Created k8s.core.v1.secret '{k8s_secret_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert_secret(data, metadata, metadata_name, ret.get("new_state"))

    resource_id = ret.get("new_state").get("resource_id")

    # Describe k8s_secret
    describe_ret = await hub.states.k8s.core.v1.secret.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "k8s.core.v1.secret.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("k8s.core.v1.secret.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert_secret(data, metadata, metadata_name, described_resource_map)

    # update k8s_secret with test flag
    updated_metadata = copy.deepcopy(metadata)
    updated_metadata["labels"] = {"name": "development-test"}
    ret = await hub.states.k8s.core.v1.secret.present(
        test_ctx,
        name=k8s_secret_temp_name,
        metadata=updated_metadata,
        resource_id=resource_id,
        data=data,
        type="Opaque",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Would update k8s.core.v1.secret '{k8s_secret_temp_name}'" in ret["comment"]
    assert_secret(data, metadata, metadata_name, ret.get("old_state"))
    assert_secret(
        data, metadata, metadata_name, ret.get("new_state"), "development-test"
    )

    # real update k8s_secret
    ret = await hub.states.k8s.core.v1.secret.present(
        ctx,
        name=k8s_secret_temp_name,
        metadata=updated_metadata,
        resource_id=resource_id,
        data=data,
        type="Opaque",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated k8s.core.v1.secret '{k8s_secret_temp_name}'" in ret["comment"]

    assert ret.get("old_state") and ret.get("new_state")
    assert_secret(data, metadata, metadata_name, ret.get("old_state"))
    assert_secret(
        data, metadata, metadata_name, ret.get("new_state"), "development-test"
    )

    # Delete k8s_secret with test flag
    ret = await hub.states.k8s.core.v1.secret.absent(
        test_ctx, name=k8s_secret_temp_name, resource_id=resource_id, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete k8s.core.v1.secret '{k8s_secret_temp_name}'" in ret["comment"]
    assert_secret(
        data, metadata, metadata_name, ret.get("old_state"), "development-test"
    )

    # Delete k8s_secret with real account
    ret = await hub.states.k8s.core.v1.secret.absent(
        ctx, name=k8s_secret_temp_name, resource_id=resource_id, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted k8s.core.v1.secret '{k8s_secret_temp_name}'" in ret["comment"]
    assert_secret(
        data, metadata, metadata_name, ret.get("old_state"), "development-test"
    )

    # Deleting k8s_secret again should be an no-op
    ret = await hub.states.k8s.core.v1.secret.absent(
        ctx, name=k8s_secret_temp_name, resource_id=resource_id, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"k8s.core.v1.secret '{k8s_secret_temp_name}' already absent" in ret["comment"]
    )


def assert_secret(
    data,
    metadata,
    metadata_name,
    resource,
    label_value="development-1",
    resource_id=None,
):
    assert data == resource.get("data")
    assert "Opaque" == resource.get("type")
    assert metadata.get("secret") == resource.get("metadata").get("secret")
    assert metadata.get("name") == resource.get("metadata").get("name")
    if not resource_id:
        assert metadata_name == resource.get("resource_id")
    else:
        assert resource_id == resource.get("resource_id")
    assert label_value == resource.get("metadata").get("labels").get("name")
