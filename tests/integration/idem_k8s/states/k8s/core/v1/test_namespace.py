import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_namespace(hub, ctx):
    k8s_namespace_temp_name = "idem-test-ns-" + str(uuid.uuid4())
    metadata_name = k8s_namespace_temp_name
    metadata = {"name": metadata_name, "labels": {"name": "development-1"}}

    # create k8s_namespace with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.k8s.core.v1.namespace.present(
        test_ctx, name=k8s_namespace_temp_name, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create k8s.core.v1.namespace '{k8s_namespace_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_namespace(
        metadata,
        metadata_name,
        ret.get("new_state"),
        "development-1",
        "resource_id_known_after_present",
    )

    # create real k8s_namespace
    ret = await hub.states.k8s.core.v1.namespace.present(
        ctx, name=k8s_namespace_temp_name, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created k8s.core.v1.namespace '{k8s_namespace_temp_name}'" in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_namespace(metadata, metadata_name, ret.get("new_state"))

    resource_id = ret.get("new_state").get("resource_id")

    # Describe k8s_namespace
    describe_ret = await hub.states.k8s.core.v1.namespace.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "k8s.core.v1.namespace.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "k8s.core.v1.namespace.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_namespace(metadata, metadata_name, described_resource_map)

    # update k8s_namespace with test flag
    updated_metadata = copy.deepcopy(metadata)
    updated_metadata["labels"] = {"name": "development-test"}
    ret = await hub.states.k8s.core.v1.namespace.present(
        test_ctx,
        name=k8s_namespace_temp_name,
        metadata=updated_metadata,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update k8s.core.v1.namespace '{k8s_namespace_temp_name}'"
        in ret["comment"]
    )
    assert_namespace(metadata, metadata_name, ret.get("old_state"))
    assert_namespace(metadata, metadata_name, ret.get("new_state"), "development-test")

    # real update k8s_namespace
    ret = await hub.states.k8s.core.v1.namespace.present(
        ctx,
        name=k8s_namespace_temp_name,
        metadata=updated_metadata,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Updated k8s.core.v1.namespace '{k8s_namespace_temp_name}'" in ret["comment"]
    )
    assert_namespace(metadata, metadata_name, ret.get("old_state"))
    assert_namespace(metadata, metadata_name, ret.get("new_state"), "development-test")

    # Delete k8s_namespace with test flag
    ret = await hub.states.k8s.core.v1.namespace.absent(
        test_ctx, name=k8s_namespace_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete k8s.core.v1.namespace '{k8s_namespace_temp_name}'"
        in ret["comment"]
    )
    assert_namespace(metadata, metadata_name, ret.get("old_state"), "development-test")

    # Delete k8s_namespace with real account
    ret = await hub.states.k8s.core.v1.namespace.absent(
        ctx, name=k8s_namespace_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted k8s.core.v1.namespace '{k8s_namespace_temp_name}'" in ret["comment"]
    )
    assert_namespace(metadata, metadata_name, ret.get("old_state"), "development-test")

    # Deleting k8s_namespace again should be an no-op
    ret = await hub.states.k8s.core.v1.namespace.absent(
        ctx, name=k8s_namespace_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"k8s.core.v1.namespace '{k8s_namespace_temp_name}' already absent"
        in ret["comment"]
    )


def assert_namespace(
    metadata, metadata_name, resource, label_value="development-1", resource_id=None
):
    assert metadata.get("name") == resource.get("metadata").get("name")
    if not resource_id:
        assert metadata_name == resource.get("resource_id")
    else:
        assert resource_id == resource.get("resource_id")
    assert label_value == resource.get("metadata").get("labels").get("name")
