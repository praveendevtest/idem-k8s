import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_daemon_set(hub, ctx):
    k8s_daemon_set_temp_name = "idem-test-k8s-daemon-set-" + str(uuid.uuid4())
    metadata = {"name": k8s_daemon_set_temp_name, "namespace": "default"}
    spec = {
        "selector": {"match_labels": {"name": k8s_daemon_set_temp_name}},
        "update_strategy": {
            "rolling_update": {"max_unavailable": 1},
            "type": "RollingUpdate",
        },
        "template": {
            "metadata": {"labels": {"name": k8s_daemon_set_temp_name}},
            "spec": {
                "containers": [
                    {
                        "image": "nginx:1.14.2",
                        "image_pull_policy": "IfNotPresent",
                        "name": "nginx",
                        "resources": {
                            "limits": {"memory": "200Mi"},
                            "requests": {"cpu": "100m", "memory": "200Mi"},
                        },
                        "termination_message_path": "/dev/termination-log",
                        "termination_message_policy": "File",
                    }
                ],
                "restart_policy": "Always",
            },
        },
    }

    # create k8s_daemon_set with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.k8s.apps.v1.daemon_set.present(
        test_ctx, name=k8s_daemon_set_temp_name, metadata=metadata, spec=spec
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create k8s.apps.v1.daemon_set '{k8s_daemon_set_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_daemon_set(
        metadata,
        ret.get("new_state"),
        spec,
        "resource_id_known_after_present",
    )

    # create real k8s_daemon_set
    ret = await hub.states.k8s.apps.v1.daemon_set.present(
        ctx, name=k8s_daemon_set_temp_name, metadata=metadata, spec=spec
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created k8s.apps.v1.daemon_set '{k8s_daemon_set_temp_name}'" in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_daemon_set(metadata, ret.get("new_state"), spec)

    resource_id = ret.get("new_state").get("resource_id")

    # Describe k8s_daemon_set
    describe_ret = await hub.states.k8s.apps.v1.daemon_set.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "k8s.apps.v1.daemon_set.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "k8s.apps.v1.daemon_set.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_daemon_set(metadata, described_resource_map, spec)

    # update k8s_daemon_set with test flag
    updated_spec = copy.deepcopy(spec)
    updated_spec["update_strategy"] = {
        "rolling_update": {"max_unavailable": 1},
        "type": "OnDelete",
    }
    ret = await hub.states.k8s.apps.v1.daemon_set.present(
        test_ctx,
        name=k8s_daemon_set_temp_name,
        metadata=metadata,
        spec=updated_spec,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update k8s.apps.v1.daemon_set '{k8s_daemon_set_temp_name}'"
        in ret["comment"]
    )

    assert ret.get("old_state") and ret.get("new_state")
    assert_daemon_set(metadata, ret.get("old_state"), spec)
    assert_daemon_set(metadata, ret.get("new_state"), updated_spec)

    # real update k8s_daemon_set
    ret = await hub.states.k8s.apps.v1.daemon_set.present(
        ctx,
        name=k8s_daemon_set_temp_name,
        metadata=metadata,
        spec=updated_spec,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Updated k8s.apps.v1.daemon_set '{k8s_daemon_set_temp_name}'" in ret["comment"]
    )

    assert ret.get("old_state") and ret.get("new_state")
    assert_daemon_set(metadata, ret.get("old_state"), spec)
    assert_daemon_set(metadata, ret.get("new_state"), updated_spec)

    # Delete k8s_daemon_set with test flag
    ret = await hub.states.k8s.apps.v1.daemon_set.absent(
        test_ctx,
        name=k8s_daemon_set_temp_name,
        resource_id=resource_id,
        metadata=metadata,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete k8s.apps.v1.daemon_set '{k8s_daemon_set_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert resource_id == resource.get("resource_id")
    assert_daemon_set(metadata, resource, updated_spec)

    # Delete k8s_daemon_set with real account
    ret = await hub.states.k8s.apps.v1.daemon_set.absent(
        ctx, name=k8s_daemon_set_temp_name, resource_id=resource_id, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted k8s.apps.v1.daemon_set '{k8s_daemon_set_temp_name}'" in ret["comment"]
    )
    assert_daemon_set(metadata, ret.get("old_state"), updated_spec)

    # Deleting k8s_daemon_set again should be an no-op
    ret = await hub.states.k8s.apps.v1.daemon_set.absent(
        ctx, name=k8s_daemon_set_temp_name, resource_id=resource_id, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"k8s.apps.v1.daemon_set '{k8s_daemon_set_temp_name}' already absent"
        in ret["comment"]
    )


def assert_daemon_set(metadata, resource, spec, resource_id=None):
    assert metadata.get("namespace") == resource.get("metadata").get("namespace")
    assert metadata.get("name") == resource.get("metadata").get("name")
    assert spec == resource.get("spec")
    if resource_id:
        assert resource_id == resource.get("resource_id")
    else:
        assert metadata.get("name") == resource.get("resource_id")
