import copy
import os

import dict_tools


def test_load_kube_config_failed(mock_hub, hub, ctx):
    # Assert load_kube_config raises exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy["acct"]["kube_config_path"] = None
    ctx_copy["acct"]["context"] = None
    os.environ.pop("KUBE_CONFIG_PATH", None)
    os.environ.pop("KUBE_CTX", None)
    try:
        hub.tool.k8s.client.load_kube_config(ctx_copy)
        assert False
    except Exception as exc:
        assert exc.message == "kube_config_path need to be set"


def test_load_kube_config_from_evn(mock_hub, hub, ctx):
    # Assert load_kube_config raises no exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy["acct"]["kube_config_path"] = None
    ctx_copy["acct"]["context"] = None
    os.environ["KUBE_CONFIG_PATH"] = ctx.acct["kube_config_path"]
    os.environ["KUBE_CTX"] = ctx.acct["context"]
    try:
        hub.tool.k8s.client.load_kube_config(ctx_copy)
    except Exception as exc:
        assert False, f"load_kube_config raised an exception {exc}"


def test_load_kube_config_current_context(mock_hub, hub, ctx):
    # Assert load_kube_config raises no exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy["acct"]["kube_config_path"] = None
    ctx_copy["acct"]["context"] = None
    os.environ["KUBE_CONFIG_PATH"] = ctx.acct["kube_config_path"]
    os.environ.pop("KUBE_CTX", None)
    try:
        hub.tool.k8s.client.load_kube_config(ctx_copy)
    except Exception as exc:
        assert False, f"load_kube_config raised an exception {exc}"


def test_load_kube_config_from_extras(mock_hub, hub, ctx):
    # Assert load_kube_config raises no exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy["acct"]["kube_config_path"] = None
    ctx_copy["acct"]["context"] = None
    os.environ.pop("KUBE_CONFIG_PATH", None)
    os.environ.pop("KUBE_CTX", None)
    hub.OPT = dict_tools.data.NamespaceDict(
        {
            "idem": {"acct_profile": "default"},
            "acct": {
                "extras": {
                    "k8s": {
                        "default": {
                            "kube_config_path": ctx.acct["kube_config_path"],
                            "context": ctx.acct["context"],
                        }
                    }
                }
            },
        }
    )
    try:
        hub.tool.k8s.client.load_kube_config(ctx_copy)
    except Exception as exc:
        assert False, f"load_kube_config raised an exception {exc}"


def test_load_kube_config_from_ctx_acct(mock_hub, hub, ctx):
    # Assert load_kube_config raises no exception.
    ctx_copy = copy.deepcopy(ctx)
    ctx_copy["acct"]["kube_config_path"] = ctx.acct["kube_config_path"]
    ctx_copy["acct"]["context"] = ctx.acct["context"]
    os.environ.pop("KUBE_CONFIG_PATH", None)
    os.environ.pop("KUBE_CTX", None)
    hub.OPT = dict_tools.data.NamespaceDict(
        {
            "idem": {"acct_profile": "default"},
            "acct": {
                "extras": {
                    "k8s": {
                        "other-profile": {  # different profile name
                            "kube_config_path": ctx.acct["kube_config_path"],
                            "context": ctx.acct["context"],
                        }
                    }
                }
            },
        }
    )
    try:
        hub.tool.k8s.client.load_kube_config(ctx_copy)
    except Exception as exc:
        assert False, f"load_kube_config raised an exception {exc}"
