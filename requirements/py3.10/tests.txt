#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.10/tests.txt requirements/tests.in
#
acct==8.5.2
    # via
    #   idem
    #   pop-evbus
aiofiles==0.8.0
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   pop-tree
asynctest==0.13.0
    # via
    #   -r requirements/tests.in
    #   pytest-pop
attrs==21.4.0
    # via pytest
cachetools==5.0.0
    # via google-auth
certifi==2021.10.8
    # via
    #   kubernetes
    #   requests
cffi==1.15.0
    # via cryptography
charset-normalizer==2.0.12
    # via requests
colorama==0.4.6
    # via
    #   idem
    #   rend
cryptography==36.0.2
    # via acct
deepdiff==5.8.0
    # via -r requirements/base.txt
dict-toolbox==3.0.3
    # via
    #   acct
    #   idem
    #   pop
    #   pop-config
    #   rend
google-auth==2.6.3
    # via kubernetes
idem==21.0.0
    # via -r requirements/base.txt
idna==3.3
    # via requests
iniconfig==1.1.1
    # via pytest
jinja2==3.1.1
    # via
    #   idem
    #   rend
jmespath==1.0.0
    # via
    #   -r requirements/base.txt
    #   idem
kubernetes==23.3.0
    # via -r requirements/base.txt
lazy-object-proxy==1.7.1
    # via pop
markupsafe==2.1.1
    # via jinja2
mock==4.0.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.3
    # via
    #   dict-toolbox
    #   pop-evbus
    #   pop-serial
nest-asyncio==1.5.5
    # via
    #   pop-loop
    #   pytest-pop
oauthlib==3.2.0
    # via requests-oauthlib
ordered-set==4.1.0
    # via deepdiff
packaging==21.3
    # via pytest
pluggy==1.0.0
    # via pytest
pop-config==12.0.1
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop
pop-evbus==6.2.3
    # via idem
pop-loop==1.0.6
    # via
    #   idem
    #   pop
pop-serial==1.1.0
    # via
    #   acct
    #   idem
    #   pop-evbus
pop-tree==9.3.0
    # via idem
pop==23.1.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-loop
    #   pop-serial
    #   pop-tree
    #   pytest-pop
    #   rend
py==1.11.0
    # via pytest
pyasn1-modules==0.2.8
    # via google-auth
pyasn1==0.4.8
    # via
    #   pyasn1-modules
    #   rsa
pycparser==2.21
    # via cffi
pyparsing==3.0.8
    # via packaging
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.18.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
pytest-pop==8.0.1
    # via -r requirements/tests.in
pytest==7.1.1
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
python-dateutil==2.8.2
    # via kubernetes
pyyaml==6.0
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   kubernetes
    #   pop
    #   rend
rend==6.5.2
    # via
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-tree
requests-oauthlib==1.3.1
    # via kubernetes
requests==2.27.1
    # via
    #   kubernetes
    #   requests-oauthlib
rsa==4.8
    # via google-auth
six==1.16.0
    # via
    #   google-auth
    #   kubernetes
    #   python-dateutil
sniffio==1.2.0
    # via pop-loop
toml==0.10.2
    # via
    #   idem
    #   rend
tomli==2.0.1
    # via pytest
tqdm==4.64.0
    # via idem
urllib3==1.26.9
    # via
    #   kubernetes
    #   requests
uvloop==0.17.0
    # via idem
websocket-client==1.3.2
    # via kubernetes
wheel==0.37.1
    # via idem

# The following packages are considered to be unsafe in a requirements file:
# setuptools
